<?php

use MediaWiki\Page\ProperPageIdentity;

class PageHistoryLookup {

	/**
	 * @param ProperPageIdentity $page Callers that have a Title will need to call toPageIdentity().
	 * @param int $limit
	 *
	 * @return mixed
	 */
	public function getHistory( ProperPageIdentity $page, $limit = 100 ) {
		// like getQueryInfo, but sane
		$query = $this->revisionStore->getSelectQueryBuilder();

		// $page is guaranteed to be proper.
		// getId will throw if $page belongs to a wiki different from the one this
		// service object is connected to.
		$pageId = $page->getId( $this->wikiId );

		$query->from( 'revision' );
		$query->where( [ 'rev_page' => $pageId ] );
		$query->orderBy( [ 'rev_timestamp', 'rev_id' ], DESC );
		$query->limit( $limit );

		$result = $query->fetchResultSet();
		$revisions = $this->revisionStore->newRevisionsFromBatch( $result );

		return $revisions;
	}

}
