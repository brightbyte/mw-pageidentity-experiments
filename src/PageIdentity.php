<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 */

namespace MediaWiki\Page;

use RuntimeException;
use Wikimedia\Assert\PreconditionException;

/**
 * Interface for objects representing an editable wiki page.
 *
 * The identity of any PageIdentity object is defined by the
 * namespace, the dbkey, and the wiki ID.
 * The page ID together with the wiki ID also identifies the page,
 * unless the page ID is 0.
 *
 * @stable to type
 *
 * @since 1.36
 */
interface PageIdentity {

	/**
	 * Get the ID of the wiki this page belongs to.
	 *
	 * @see RevisionRecord::getWikiId()
	 *
	 * @return string|false The wiki's logical name, of false to indicate the local wiki.
	 */
	public function getWikiId();

	/**
	 * Throws if $wikiId is different from the return value of getWikiId().
	 *
	 * @param string|false $wikiId The wiki ID expected by the caller.
	 *
	 * @throws PreconditionException If $wikiId is not the ID of the wiki this PageIdentity
	 *         belongs to.
	 */
	public function assertWiki( $wikiId );

	/**
	 * Returns the page ID.
	 *
	 * If this ID is 0, this means the page does not exist.
	 *
	 * Implementations must call assertWiki().
	 *
	 * @param string|false $wikiId Must be provided when accessing the ID of a non-local
	 *        PageIdentity, to prevent data corruption when using a PageIdentity belonging
	 *        to one wiki in the context of another. Should be omitted if expecting the local wiki.
	 *
	 * @return int
	 * @throws RuntimeException if this PageIdentity is not a "proper"
	 *         page identity, but e.g. a relative section link, an interwiki
	 *         link, etc.
	 * @throws RuntimeException if this PageIdentity does not belong to the wiki
	 *         identified by $wikiId.
	 * @see Title::getArticleID()
	 * @see Title::toPageIdentity()
	 * @see canExist()
	 *
	 */
	public function getId( $wikiId = false );

	/**
	 * Checks whether this PageIdentity represents a "proper" page,
	 * meaning that it could exist as an editable page on the wiki.
	 *
	 * @note this method is guaranteed to return true.
	 *
	 * @deprecated redundant, to be removed
	 *
	 * @return bool true
	 *
	 * @see getId()
	 */
	public function canExist();

	/**
	 * Checks if the page currently exists.
	 *
	 * Implementations must ensure that this method returns false
	 * when getId() would throw or return 0.
	 *
	 * @see Title::exists()
	 *
	 * @return bool
	 */
	public function exists();

	/**
	 * Returns the page's namespace number.
	 *
	 * The value returned by this method should represent a valid namespace,
	 * but this cannot be guaranteed in all cases.
	 *
	 * @return int
	 */
	public function getNamespace();

	/**
	 * Get the page title in DB key form.
	 *
	 * @return string
	 */
	public function getDBkey();

	/**
	 * Returns an informative human readable representation of the page identity,
	 * for use in logging and debugging.
	 *
	 * @return string
	 */
	public function __toString();

}

class_alias( PageIdentity::class, 'MediaWiki\Page\ProperPageIdentity' );
